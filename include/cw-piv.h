/*
  piv definitions for card-whisperer

  (C)Copyright 2022-2024 Smithee Solutions LLC

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

// this is defined in NIST SP-800-73-4 Part 1 Section 2.2

#define CW_PIV_APPLICATION_IDENTIFIER { 0x00, 0xA4, 0x04, 0x00,  0x09, 0xA0, 0x00, 0x00, 0x03, 0x08, 0x00, 0x00,  0x10, 0x00, 0x00 }

/*
  This is the 7816 command to get the CHUID.

  CLA=00
  INS=CB (Get Data)
  P1,P2=3FFF
  Lc=5
  Payload:
    tag 5C
    3 bytes
    contents is the ident for the CHUID (5F-C1-02)
  Le=0

  Note: the ident is from Table 3 of 800-73-4 Part 1, 
*/

#define CW_PIV_GET_DATA_CHUID { 0x00, 0xCB, 0x3F, 0xFF, 0x05, 0x5C, 0x03, 0x5F, 0xC1, 0x02, 0x00 }



// other application identifier values...

// { 0x00, 0xA4, 0x04, 0x00, 0x0A, 0xA0, 0x00, 0x00, 0x00, 0x62, 0x03, 0x01, 0x0C, 0x06, 0x01 };
// ?twic    { 0x00, 0xA4, 0x04, 0x00, 0x09, 0xA0, 0x00, 0x00, 0x03, 0x08, 0x00, 0x00, 0x10, 0x00, 00 };
// 0x00, 0xA4, 0x04, 0x00, 0x0F, 0xD2, 0x33, 0x00, 0x00, 0x00, 0x45, 0x73, 0x74, 0x45, 0x49, 0x44, 0x20, 0x76, 0x33, 0x35

