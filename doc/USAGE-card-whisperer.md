---
title: card-whisperer
include-before:
header-includes: |
  \usepackage{fancyhdr}
  \pagestyle{fancy}
  \fancyfoot[CO,CE]{card-whisperer usage}
  \fancyfoot[LE,RO]{\thepage}
include-before:
- '`\newpage{}`{=latex}'
---

Version 1.2.3

\newpage{}

Introduction
============

tbd

Installation
============

Note that with some smartcard readers you need to disable loading the NFC
modules.  create a blacklist file containing this lines.  The filename is
/etc/modprobe.d/blacklist.conf.

```text
  install nfc /bin/false
  install pn533 /bin/false
```

Tools
=====

dump-fascn
----------

dump-fascn dumps the (hex as argument) 75-bit version of the FASC-N.

Takes one argument, the 75-bit FASC-N as hex.

lscard
------

lists everything on the card

--verbosity=99

--chuid writes card_chuid.bin

--reader specifies reader index (useful with contact/contactless readers)

```text
  unwind <hex of FASC-N>
```

<<<<<<< HEAD
## read-fascn ##

Given a full FASC-N decodes and displays the values.

Requires input as single bytes separated by spaces

### Usage ###

``` read-fascn x <fascn octet 1> <fascn octet 2> ... <fascn last octet> ```

=======

## Colophon ##

  portions based on Ludovic Rousseau's blog post
    http://ludovicrousseau.blogspot.com/2010/04/pcsc-sample-in-c.html

