---
title: Builing card-whisperer
---

\newpage {}

# Required Components #

- Raspberry Pi 4
- blank 32gig sd card
- Rasbian 64 bit 15mar2024 kernel 6.6 debian 12 (bookworm)
- USB smartcard reader suitable for use with PCSC

Note:
You can build this on other systems.  These instructions work on Ubuntu or 
Debian systems.  

# Assumptions #

- This is running on a Pi 4 using Raspbian or an equivalent Debian-like
system.
- This is done as user 'opsadmin1'.
- opsadmin1 has sudo.
- The public internet is visible to the system.
- openssl 1.1.1v is used.

# 1. Platform Set-up #

This assumes you are setting up a Raspbian system from scratch.

- boot raspbian.
- change to US keyboard
(other -> US -> "English (US" at the top.)
- add a user ('opsadmin1')
- log in as opsadmin1
- sudo raspi-config
- enable ssh
- set the locale for country US
- set the time zone to UTC

- prepare some directories:

```
 sudo mkdir /opt/tester /opt/crypto
 sudo chown opsadmin1:opsadmin1 /opt/tester /opt/crypto
```

# 2. Install packages #

```
apt-get update -y
apt-get upgrade -y
apt-get install -y build-essential clang gdb git lldb
apt-get install -y libjansson-dev libpcsclite-dev opensc pcscd zlib1g-dev

```

If you want to build the documentation add pandoc

```
apt-get install -y texlive-full pandoc
```

# 3. Install OpenSSL 1.1 #

Historically this relied on now-deprecated features in OpenSSL.  It still
has some openssl artifacts.  Fetch and build the latest OpenSSL 1.1.

# 4. Install Openssl from source #

Get openssl 1.1 and build it.  (This is needed for DES support.)
Openssl source is placed in /opt/crypto.  Openssl is built to be
installed in /opt/crypto.

```
sudo mkdir -p /opt/crypto
sudo chown opsadmin1:opsadmin1 /opt/crypto
cd /opt/crypto
tar xvf openssl-OpenSSL_1_1_1v.tar.gz
cd openssl
./config --prefix=/opt/crypto
make
make install
```

# 5. Build card-whisperer from source #

```
cd ~opsadmin1
git clone https://bitbucket.org/smithee-solutions/card-whisperer
cd card-whisperer
make 2>stderr build
```
# 6. Install card-whisperer #

Confirm there are no errors (check stderr) and create a tar file
of the 'opt' directory.  Create /opt/tester with permissions for your username.
Unpack the tar from the root:

```
tar czvf /tmp/built_card-whisperer.tgz opt
sudo mkdir /opt/tester
sudo chown opsadmin1:opsadmin1 /opt/tester
cd /
tar xvf /tmp/built_card-whisperer.tgz
```

# 7. Verify Card Access #

Confirm the reader is detected:

```
opensc-tool --list-readers
```

Place the sample card on/in the reader.  Confirm the 
card is detected.  This returns
the "Answer to Reset" (ATR.)  Note which reader was used (numbered from 0.)

```
opensc-tool --atr
```

# 8. Access the PIV Applet #

Test using 'lscard'.  With the card still presented to the reader 
do

```
lscard --verbosity=99 --reader 1
```

