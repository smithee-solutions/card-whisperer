### Changes in 1.32 ###

enhance chuid writer

### Changes in 1.31 ###

updated doc to explain "dump-fascn" for 75-bit vs. "read-fascn" for full FASC-N decode.

### Changes in release 1.3 ###

switch to this release notice format
deprecate OES test code
migrate card calculator to openbadger
document build process

### Changes in 1.2.2. ###

added card whisperer context

