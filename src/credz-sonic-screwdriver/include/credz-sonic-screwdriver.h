/*
  credz-sonic-screwdriver.h - definitions for the various wrenches
*/

// CSS - credz sonic screwdriver

#define EQUALS ==
#define ST_OK                      ( 0)
#define ST_CSS_NFC_INIT            ( 1)
#define ST_CSS_NFC_OPEN            ( 2)
#define ST_CSS_TAGS                ( 3)
#define ST_CSS_FREEFARE_CONNECT    ( 4)
#define ST_CSS_KEY_WRONG_VERSION   ( 5)
#define ST_CSS_SELECT              ( 6)
#define ST_CSS_AUTH_1              ( 7)
#define ST_CSS_CHANGE_KEY_1        ( 8)
#define ST_CSS_AUTH_MASTER_FAILED  ( 9)
#define ST_CSS_AUTH_MASTER_3DES    (10)
#define ST_CSS_AUTH_MASTER_AES     (11)
#define ST_CSS_AUTH_MASTER_SUCCESS (12)
#define ST_CSS_FORMAT_FAILURE      (13)
#define ST_CSS_ALLOC_MASTER_3DES   (14)
#define ST_CSS_ALLOC_MASTER_AES    (15)
#define ST_CSS_AID_ERROR           (16)
#define ST_CSS_UNKNOWN_COMMAND     (17)
#define ST_CSS_CFILE_SEL           (18)
#define ST_CSS_CFILE               (19)
#define ST_CSS_ALLOC_KEY           (20)
#define ST_CSS_CFILE_AUTH          (21)
#define ST_CSS_CREATE_FILE         (22)
#define ST_CSS_GET_FILE_IDS        (23)
#define ST_CSS_LSFILE_SEL          (24)
#define ST_CSS_LSFILE_AUTH         (25)
#define ST_CSS_WRFILE_SEL          (26)
#define ST_CSS_WRFILE_AUTH         (27)
#define ST_CSS_WRITE               (28)
#define ST_CSS_RDFILE_SEL          (29)
#define ST_CSS_RDFILE_AUTH         (30)
#define ST_CSS_READ                (31)
#define ST_CSS_LIST_INIT           (32)
#define ST_CSS_LIST_CONNECT        (33)
#define ST_CSS_LIST_VERSION        (34)
#define ST_CSS_LIST_CLOSE          (35)
#define ST_CSS_LIST_OPEN           (36)
#define ST_CSS_FREEFARE_DISCONNECT (37)
#define ST_CSS_DEVICE_NOT_INIT     (38)
#define ST_CSS_KEY_NOT_INITIALIZED (39)
#define ST_CSS_UPDATE_NEW          (40)
#define ST_CSS_USELECT             (41)
#define ST_CSS_UAUTH_AID           (42)
#define ST_CSS_UAUTH_SETKEY        (43)
#define ST_CSS_U_CHANGE            (44)
#define ST_CSS_SETTINGS_KEY_AES    (47)
#define ST_CSS_KEY_UNINIT          (46)
#define ST_CSS_SETTINGS_KEY_3DES   (47)
#define ST_CSS_AUTH_MASTER_FAIL    (48)
#define ST_CSS_INIT_3DES           (49)
#define ST_CSS_INIT_AES            (50)
#define ST_CSS_INIT_AES_0          (51)

#define CSS_CMD_NOOP               ( 0)
#define CSS_CMD_LIST               ( 1)
#define CSS_CMD_INITIALIZE         ( 2)
#define CSS_CMD_HELP               ( 3)
#define CSS_CMD_FORMAT             ( 4)
#define CSS_CMD_CREATE_APPLICATION ( 5)
#define CSS_CMD_LS_AID             ( 6)
#define CSS_CMD_CREATE_FILE        ( 7)
#define CSS_CMD_LS_FILE            ( 8)
#define CSS_CMD_WRITE              ( 9)
#define CSS_CMD_READ               (10)
#define CSS_CMD_UPDATE_KEYMAT      (11)

#define CSS_OPT_HELP           ( 1)
#define CSS_OPT_FORMAT         ( 2)
#define CSS_OPT_INITIALIZE     ( 3)
#define CSS_OPT_LIST           ( 4)
#define CSS_OPT_VERBOSITY      ( 5)
// 6
#define CSS_OPT_AID            ( 7)
#define CSS_OPT_CREATE_APP     ( 8)
#define CSS_OPT_LS_AID         ( 9)
#define CSS_OPT_SIZE           (10)
#define CSS_OPT_FILE_ID        (11)
#define CSS_OPT_AUTH_AID       (12)
#define CSS_OPT_CREATE_FILE    (13)
#define CSS_OPT_LS_FILE        (14)
#define CSS_OPT_DATA           (15)
#define CSS_OPT_WRITE          (16)
#define CSS_OPT_READ           (17)
#define CSS_OPT_UPDATE_KEYMAT  (18)

#define CSS_MAX_NFC_DEVICES (8)

#define CSS_KEYTYPE_TRIPLEDES (1)
#define CSS_KEYTYPE_AES128    (2)
#define CSS_KEYTYPE_AES128_OCTETS (128/8)


typedef struct css_context
{
  int verbosity;

  int was_app_selected;
  int was_auth_app;
  int was_auth_picc;
  int was_device_initialized;
  int was_init_keys;
  int was_open_desfire;

  int command;
  int device_count;
  int status_freefare;
  FreefareTag tag;
  int key_number;
  int have_aid;
  uint32_t aid;
  int aid_auth_key;
  int file_id;
  int size;
  char data [8*1024];
  char settings_file_path [1024];
} CSS_CONTEXT;

#define CSS_KEY_SLOT_MAX     (16)
#define CSS_KEY_SLOT_IDLE    (0x00)
#define CSS_KEY_SLOT_ACTIVE  (0x01)

//0-3
#define CSS_KEY_AES_DEFAULT  ( 4)
#define CSS_KEY_3DES_DEFAULT ( 5)
#define CSS_KEY_NEW_PICC_AES ( 6)
//7
#define CSS_KEY_PICC_MASTER  ( 8)
#define CSS_KEY_AID_AUTH     ( 9)
//10-15


typedef struct css_key_material
{
  int state;
  int key_type;
  int key_id; // used in the desfire card
  unsigned char key [128/8];
  MifareDESFireKey freefare_key;
} CSS_KEY_MATERIAL;


int css_authenticate_picc(CSS_CONTEXT *ctx);
int css_close_desfire(CSS_CONTEXT *ctx);
int css_create_application(CSS_CONTEXT *ctx);
int css_create_file(CSS_CONTEXT *ctx);
int css_format_desfire(CSS_CONTEXT *ctx);
int css_hex_to_octets(CSS_CONTEXT *ctx, char *hex_string, char *octet_buffer, int *octet_count);
int css_initialize_desfire(CSS_CONTEXT *ctx);
int css_list_applications(CSS_CONTEXT *ctx);
int css_list_files(CSS_CONTEXT *ctx);
int css_list_desfire (CSS_CONTEXT *ctx);
int css_list_uid(CSS_CONTEXT *ctx);
int css_open_desfire (CSS_CONTEXT *ctx);
int css2_open_desfire (CSS_CONTEXT *ctx);
int css_read_data (CSS_CONTEXT *ctx);
int css_read_settings(CSS_CONTEXT *ctx);
char *css_status_message(int status);
int css_write_data (CSS_CONTEXT *ctx);
int css_update_keymat(CSS_CONTEXT *ctx);

