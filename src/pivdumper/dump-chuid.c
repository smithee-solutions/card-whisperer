#include <stdio.h>
#include <string.h>


#include <PCSC/wintypes.h>
#include <PCSC/pcsclite.h>
#include <PCSC/winscard.h>

#include <card-whisperer.h>

BYTE card_buffer [16384];

int main
  (int argc,
  char *argv [])

{
  CSSH_CONFIG config;
  unsigned char chuid_buffer [16384];
  FILE *chuid_data;
  int status;
  int status_io;


  status = 0;
  memset(&config, 0, sizeof(config));
  config.verbosity = 9;
  config.log = stderr;
  chuid_data = fopen(argv[1], "r");
  if (chuid_data EQUALS NULL)
    status = -1;
  if (status EQUALS 0)
  {
    status_io = fread(chuid_buffer, sizeof(chuid_buffer[0]), sizeof(chuid_buffer), chuid_data);
    if (status_io < 1)
      status = -2;
  };
  if (status EQUALS 0)
  {
    fprintf(stderr, "CHUID file is %d. bytes\n", status_io);
    status = dump_card_data(&config, chuid_buffer, status_io);
  };
  return(status);
}
