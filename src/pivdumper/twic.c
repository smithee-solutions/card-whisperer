int verbosity=9;
// output equivalent of TWIC wiegand data

#include <stdio.h>
#include <string.h>

#include <card-whisperer-version.h>
#define EQUALS ==
char *int_to_bits(unsigned long int value, int low_order_bits, int high_order_bits);
void parity_over_bits(char *bitstring, int front_bits, int back_bits, unsigned int *front_even, unsigned int *back_odd);
int sum_bits(char *bitstring, int start_bit, int bit_length);

int main (int argc, char *argv [])
{

  unsigned int agency_code;
  unsigned int back_parity;
  char bitstring [76];
  unsigned long int credential_number;
  unsigned long int expiration_date;
  unsigned int front_parity;
  unsigned int system_code;
  char wiegand_bits [76];

#define AGENCY_CODE_BITS (14)
#define CREDENTIAL_NUMBER_BITS (20)
#define EXPIRATION_DATE_BITS (25)
#define SYSTEM_CODE_BITS (14)


  printf("twic - fabricate a 75-bit TWIC value.  Part of %s\n", CSHH_VERSION_STRING);
  wiegand_bits [0] = 0;
  front_parity = 1;
  back_parity = 1;

  agency_code = 1;
  system_code = 2;
  credential_number = 3;
  expiration_date=20220101;
  if (argc > 4)
  {
    sscanf(argv[4], "%ld", &expiration_date);
    if (argc > 3)
    {
      sscanf(argv[3], "%ld", &credential_number);
      if (argc > 2)
      {
        sscanf(argv[2], "%u", &system_code);
        if (argc > 1)
        {
          sscanf(argv[1], "%u", &agency_code);
        };
      };
    };
  };

  fprintf(stderr, "Agency Code: %04d\n", (int)agency_code);
  fprintf(stderr, "System Code: %04d\n", (int)system_code);
  fprintf(stderr, " Credential: %d\n", (int)credential_number);
  fprintf(stderr, " Expiration: %08d\n", (int)expiration_date);

  sprintf(bitstring, "%d", front_parity);
  strcat(wiegand_bits, bitstring);
  sprintf(bitstring, "%s", int_to_bits((unsigned long int)agency_code, AGENCY_CODE_BITS, 0));
  strcat(wiegand_bits, bitstring);
  sprintf(bitstring, "%s", int_to_bits((unsigned long int)system_code, SYSTEM_CODE_BITS, 0));
  strcat(wiegand_bits, bitstring);
  sprintf(bitstring, "%s", int_to_bits((unsigned long int)credential_number, CREDENTIAL_NUMBER_BITS, 0));
  strcat(wiegand_bits, bitstring);
  sprintf(bitstring, "%s", int_to_bits((unsigned long int)expiration_date, EXPIRATION_DATE_BITS, 0));
  strcat(wiegand_bits, bitstring);
  sprintf(bitstring, "%d", back_parity);
  strcat(wiegand_bits, bitstring);
  parity_over_bits(wiegand_bits, 37, 36, &front_parity, &back_parity);
  fprintf(stderr, "Front 37 even %d", front_parity);
  fprintf(stderr, " Back 36 odd %d\n", back_parity);
  if (front_parity)
    wiegand_bits [0] = '1';
  else
    wiegand_bits [0] = '0';
  if (back_parity)
    wiegand_bits [74] = '1';
  else
    wiegand_bits [74] = '0';

  printf("75 Bit Wiegand should be %s\n", wiegand_bits);
  return(0);
}

char *int_to_bits(unsigned long int value, int low_order_bits, int high_order_bits)
{
  int i;
  int position;
  static char results [80];

  memset(results, 0, sizeof(results));
  if (high_order_bits > 0)
  {
    // we don't do that
  };
  if (low_order_bits > 0)
  {
    unsigned long int mask;
    mask = 1;
    for (i=0; i<low_order_bits; i++)
    {
      position = low_order_bits - i -1;
      if (mask & value)
        results [position] = '1';
      else
        results [position] = '0';
      mask = mask << 1;
    };
  };
  if (verbosity > 3)
    fprintf(stderr, "int_to_bits returns %s for %lu(lo %d hi %d)\n", results, value, low_order_bits, high_order_bits);

  return(results);
}

void parity_over_bits
  (char *bitstring,
  int front_bits,
  int back_bits,
  unsigned int *front_even,
  unsigned int *back_odd)

{ /* parity_over_bits */
  *back_odd = 0;
  *front_even = 0;
  if (!(sum_bits(bitstring, 0, front_bits) & 1))
  {
    *front_even = 1;
  };
  if (sum_bits(bitstring, strlen(bitstring)-back_bits, back_bits) & 1)
  {
    *back_odd = 1;
  };

} /* parity_over_bits */


int sum_bits(char *bitstring, int start_bit, int bit_length)
{
  int i;
  int sum_of_bits;

  sum_of_bits = 0;
  for (i=start_bit; i< start_bit + bit_length; i++)
  {
    if (bitstring [i] EQUALS '1')
      sum_of_bits++;
  };

  if (verbosity > 3)
    fprintf(stderr, "sum of bits was %d (bitstring start %d lth %d\n", sum_of_bits, start_bit, bit_length);
  return(sum_of_bits);
}

