int verbosity=9;

// output equivalent of TWIC wiegand data

#include <stdio.h>
#include <string.h>

#include <card-whisperer-version.h>
#define EQUALS ==
#define WIEGAND_OUTPUT_LENGTH (75)
#define AGENCY_CODE_BITS (14)
#define CREDENTIAL_NUMBER_BITS (20)
#define EXPIRATION_DATE_BITS (25)
#define SYSTEM_CODE_BITS (14)
unsigned long int bits_to_number(char *bitstring);
char *int_to_bits(unsigned long int value, int low_order_bits, int high_order_bits);
void parity_over_bits(char *bitstring, int front_bits, int back_bits, unsigned int *front_even, unsigned int *back_odd);
int sum_bits(char *bitstring, int start_bit, int bit_length);

int main (int argc, char *argv [])
{

  unsigned int agency_code;
  unsigned int back_parity;
  unsigned long int credential_number;
  unsigned long int expiration_date;
  char field_string [1+WIEGAND_OUTPUT_LENGTH];
  unsigned int front_parity;
  unsigned int system_code;
  char wiegand_bits [1+WIEGAND_OUTPUT_LENGTH];


  printf("untwic - unwind a 75-bit TWIC value.  Part of %s\n", CSHH_VERSION_STRING);
  memset(wiegand_bits, 0x31, sizeof(wiegand_bits));
  wiegand_bits [sizeof(wiegand_bits)-1] = 0;
  if (argc > 1)
  {
    if (strlen(argv [1]) != WIEGAND_OUTPUT_LENGTH)
      fprintf(stderr, "input string wrong length.  expecting %d. got %d.\n", WIEGAND_OUTPUT_LENGTH, (int)strlen(argv[1]));
    else
      strcpy(wiegand_bits, argv [1]);
  };
  fprintf(stderr, "Decoding Wiegand Bitstring %s\n", wiegand_bits);
  if (wiegand_bits [0] EQUALS '1')
    front_parity = 1;
  else
    front_parity = 0;
  if (wiegand_bits [74] EQUALS '1')
    back_parity = 1;
  else
    back_parity = 0;

  strncpy(field_string, wiegand_bits+1, AGENCY_CODE_BITS);
  agency_code = bits_to_number(field_string);
  strncpy(field_string, wiegand_bits+1+AGENCY_CODE_BITS, SYSTEM_CODE_BITS);
  system_code = bits_to_number(field_string);
  strncpy(field_string, wiegand_bits+1+AGENCY_CODE_BITS+SYSTEM_CODE_BITS, CREDENTIAL_NUMBER_BITS);
  credential_number = bits_to_number(field_string);
  strncpy(field_string, wiegand_bits+1+AGENCY_CODE_BITS+SYSTEM_CODE_BITS+CREDENTIAL_NUMBER_BITS, EXPIRATION_DATE_BITS);
  expiration_date = bits_to_number(field_string);

  fprintf(stderr, "Front Parity (Even): %d\n", front_parity);
  fprintf(stderr, "Agency Code: %04d\n", (int)agency_code);
  fprintf(stderr, "System Code: %04d\n", (int)system_code);
  fprintf(stderr, " Credential: %d\n", (int)credential_number);
  fprintf(stderr, " Expiration: %08d\n", (int)expiration_date);
  fprintf(stderr, "Back Parity (Odd): %d\n", back_parity);

  return(0);
}


unsigned long int bits_to_number
  (char *bitstring)

{ /* bits_to_number */

  unsigned long int answer;
  char *bit;


  if (verbosity > 3)
    fprintf(stderr, "converting %s to a number...\n", bitstring);
  answer = 0;
  bit = bitstring;
  while ((*bit) != 0)
  {
    if (*bit EQUALS '1')
      answer++;
    bit++;
    if ((*bit) != 0)
      answer = answer * 2;
  };
  if (verbosity > 3)
    fprintf(stderr, "converted %s to %lu\n", bitstring, answer);
  return(answer);
}


char *int_to_bits(unsigned long int value, int low_order_bits, int high_order_bits)
{
  int i;
  int position;
  static char results [80];

  memset(results, 0, sizeof(results));
  if (high_order_bits > 0)
  {
    // we don't do that
  };
  if (low_order_bits > 0)
  {
    unsigned long int mask;
    mask = 1;
    for (i=0; i<low_order_bits; i++)
    {
      position = low_order_bits - i -1;
      if (mask & value)
        results [position] = '1';
      else
        results [position] = '0';
      mask = mask << 1;
    };
  };

  return(results);
}

void parity_over_bits
  (char *bitstring,
  int front_bits,
  int back_bits,
  unsigned int *front_even,
  unsigned int *back_odd)

{ /* parity_over_bits */
  *back_odd = 0;
  *front_even = 0;
  if (sum_bits(bitstring, 0, front_bits) & 2)
  {
    *front_even = 1;
  };
  if (sum_bits(bitstring, strlen(bitstring)-back_bits, back_bits) & 1)
  {
    *back_odd = 1;
  };

} /* parity_over_bits */


int sum_bits(char *bitstring, int start_bit, int bit_length)
{
  int i;
  int sum_of_bits;

  sum_of_bits = 0;
  for (i=start_bit; i< start_bit + bit_length; i++)
  {
    if (bitstring [i] EQUALS '1')
      sum_of_bits++;
  };

  return(sum_of_bits);
}

