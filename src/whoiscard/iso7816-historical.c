// (C)Copyright 2022 Smithee Solutions LLC

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <PCSC/wintypes.h>
#include <PCSC/pcsclite.h>
#include <PCSC/winscard.h>


#include <card-whisperer.h>
#define LOG stdout


int
  interpret_historical
    (CSHH_CONTEXT *ctx,
    unsigned char *buffer,
    int count)

{ /* interpret_historical */

  unsigned char *hptr;
  int i;
  int status;


  status = ST_OK;
  hptr = buffer;
  for (i=0; i<count; i++)
  {
    fprintf(LOG, "Historical %02x:%02x\n", i, *hptr);
    hptr++;
  };

  status = interpret_historical_oberthur(ctx, buffer, count);
  return (status);

} /* interpret_historical */

