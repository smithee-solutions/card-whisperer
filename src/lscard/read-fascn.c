/*
  read-fascn.c - FASC-N Display Utility

  Usage
    read-fascn <hex string>

  must be exact number of octets

  (C)Copyright 2017-2024 Smithee Solutions LLC

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/


#include <stdio.h>
#include <string.h>

#include <PCSC/wintypes.h>
#include <PCSC/pcsclite.h>
#include <PCSC/winscard.h>


#include <eac-encode.h>
#include <card-whisperer.h>


int main
  (int argc,
  char *argv [])

{ /* main for read-fascn.c */

  CSSH_CONFIG config;
  unsigned char fasc_n [FASCN_ARRAY];
  int fascn_length;
  int status;


  status = ST_OK;
  memset (fasc_n, 0, sizeof (fasc_n));
  if (argc EQUALS 2)
  {
    fascn_length = sizeof(fasc_n);
    status = eac_encode_hex_string_to_bytes(argv [1], fasc_n, &fascn_length);
    if (status EQUALS ST_OK)
      decode_fascn (&config, fasc_n);
  }
  else
  {
    status = -1;
    fprintf(stderr, "wrong number of arguments (got %d. expecting 2)\n",
      argc);
  };

  if (status != 0)
    fprintf (stderr, "Exit status %d\n", status);
  return (status);

} /* main for read-fascn.c */

